#pragma

#include "token.hpp"
#include "builder.hpp"

class Expr;
class Stmt;
class Decl;
class Type;

/// Defines and implements semantic/parsing actions.
class Actions
{
public:
  Expr* on_boolean_literal(Token const& tok);
  /// Returns an integer expression.

  Expr* on_integer_literal(Token const& tok);
  /// Returns an integer expression.

  Expr* on_id_expression(Token const& tok);
  /// Returns an id-expression.

  Expr* on_negation_expression(Expr* arg);
  Expr* on_reciprocal_expression(Expr* arg);
  Expr* on_multiplication_expression(Expr* e1, Expr* e2);
  Expr* on_division_expression(Expr* e1, Expr* e2);
  Expr* on_remainder_expression(Expr* e1, Expr* e2);

  Decl* on_object_declaration(Token id, Type* t);
  Decl* finish_object_declaration(Decl* d, Type* t);

  Decl* on_function_declaration(Token id, std::vector<Decl*> const& parms, Type* t);
  Decl* start_function_declaration(Decl* d);
  Decl* finish_function_declaration(Decl* d, Stmt* s);

  // Scope stuff

  void enter_scope() { m_stack.emplace_back(); }
  /// Enter a new scope.
  
  void leave_scope() { m_stack.pop_back(); }
  /// Leave the current scope.
  
  Scope* get_current_scope() { return m_stack.back(); }
  /// Return the current scope.


private:
  Builder m_builder;

  Scope_stack m_stack;
  /// The scope stack.
};

#pragma once

#include "value.hpp"
#include "store.hpp"
#include "frame.hpp"

class Expr;
class Stmt;
class Decl;

/// The evaluation class provides context for the evaluation of an 
/// expression.
class Evaluation
{
public:
  Evaluation() = default;
  Evaluation(Evaluation const& x) = delete;
  Evaluation(Evaluation&& x) = delete;

  // Storage and objects

  Monotonic_store& get_globals() { return m_globals; }
  /// Returns the global store.

  Object* allocate_static(Decl* d);
  /// Allocate static storage for `d`.

  Object* allocate_automatic(Decl* d);
  /// Allocate automatic storage for `d` in the current frame.

  Object* locate_object(Value const& val);
  /// Locate the object for the given address value.

  // Call stack

  Call_stack& get_stack() { return m_stack; }
  /// Returns the call stack.

  Decl* get_current_function() const;
  /// Returns the current function.

  Frame* get_current_frame() const { return m_stack.get_top(); }
  /// Returns the current stack frame.

  Frame* push_frame(Decl* fn);
  /// Push a new stack frame for fn onto the call stack.

  void pop_frame();
  /// Pops the current stack frame.

private:
  Monotonic_store m_globals;
  /// The static store contains all objects with static storage duration.

  Call_stack m_stack;
  /// The call stack. Note that this is empty until a function is called.
};


// Operations

// Value evaluate_expr(Expr const* e);
/// Evaluate the expression `e`.

Value evaluate_expr(Evaluation& eval, Expr const* e);
/// Evaluate an expression in the given context.


/// Determines flow control after executing a statement.
enum Control
{
  next_fc,
  break_fc,
  cont_fc,
  return_fc,
};

Control evaluate_stmt(Evaluation& eval, Stmt const* e);
/// Evaluate an statement in the given context.

void evaluate_decl(Evaluation& eval, Decl const* e);
/// Evaluate a declaration in the given context.


#include "evaluation.hpp"
#include "type.hpp"
#include "expr.hpp"
#include "decl.hpp"

#include <iostream>

// Value
// evaluate_expr(Expr const* e)
// {
//   Evaluation eval;
//   return evaluate_expr(eval, e);
// }

/// The value of a literal is its value.
static Value
eval_literal(Evaluation& eval, Literal_expr const* e)
{
  return e->get_value();
}

static Value
eval_id(Evaluation& eval, Id_expr const* e)
{
  Decl* d = e->get_declaration();
  
  if (d->is_function())
    return Value(static_cast<Fn_decl*>(d));

  // FIXME: We need to know if d is global or local. For now,
  // I'm going to assume local. We need to track decl context
  // and other properties in order to make this right.
  Var_decl* var = static_cast<Var_decl*>(d);
  Frame* f = eval.get_current_frame();
  return Value(Addr_value{f->get_index(), var});
}

template<typename Op>
static Value
eval_binary_op(Evaluation& eval, Binary_expr const* e, Op op)
{
  Value v1 = evaluate_expr(eval, e->get_child(0));
  Value v2 = evaluate_expr(eval, e->get_child(1));
  if (e->get_type()->is_integral())
    return Value(op(v1.get_int(), v2.get_int()));
  else if (e->get_type()->is_float())
    return Value(op(v1.get_float(), v2.get_float()));
  else
    assert(false);
}

// FIXME: This is less elegant than I'd like.
template<>
Value
eval_binary_op<std::modulus<>>(Evaluation& eval, Binary_expr const* e, std::modulus<> op)
{
  Value v1 = evaluate_expr(eval, e->get_child(0));
  Value v2 = evaluate_expr(eval, e->get_child(1));
  if (e->get_type()->is_integral())
    return Value(op(v1.get_int(), v2.get_int()));
  else
    assert(false);
}

template<typename Op>
static Value
eval_binary_op(Evaluation& eval, Expr const* e, Op op)
{
  return eval_binary_op(eval, static_cast<Binary_expr const*>(e), op);
}

template<typename T = void>
struct invert
{
  T operator()(T const& x) const { return T(1) / x; }
};

template<>
struct invert<void>
{
  template<typename T>
  T operator()(T const& x) const { return T(1) / x; }
};

template<typename Op>
static Value
eval_unary_op(Evaluation& eval, Unary_expr const* e, Op op)
{
  Value v1 = evaluate_expr(eval, e->get_child(0));
  if (e->get_type()->is_integral())
    return Value(op(v1.get_int()));
  else if (e->get_type()->is_float())
    return Value(op(v1.get_float()));
  else
    assert(false);
}

template<typename Op>
static Value
eval_unary_op(Evaluation& eval, Expr const* e, Op op)
{
  return eval_unary_op(eval, static_cast<Unary_expr const*>(e), op);
}

template<typename Comp>
static Value
eval_relation(Evaluation& eval, Binary_expr const* e, Comp cmp)
{
  Value v1 = evaluate_expr(eval, e->get_child(0));
  Value v2 = evaluate_expr(eval, e->get_child(1));
  if (e->get_type()->is_integral())
    return Value(cmp(v1.get_int(), v2.get_int()));
  else if (e->get_type()->is_float())
    return Value(cmp(v1.get_float(), v2.get_float()));
  else
    assert(false);
}

template<typename Comp>
static Value
eval_relation(Evaluation& eval, Expr const* e, Comp cmp)
{
  return eval_relation(eval, static_cast<Binary_expr const*>(e), cmp);
}

static Value
eval_cond(Evaluation& eval, Cond_expr const* e)
{
  Value v1 = evaluate_expr(eval, e->get_condition());
  if (v1.get_int())
    return evaluate_expr(eval, e->get_true_value());
  else
    return evaluate_expr(eval, e->get_false_value());
}

static Value
eval_call(Evaluation& eval, Call_expr const* e)
{
  // Get the function being called.
  Value fval = evaluate_expr(eval, e->get_function());
  Fn_decl* fn = fval.get_function();

  // Evaluate the arguments in the calling context. 
  // We'll initialize arguments after allocating the frame.
  std::vector<Value> args;  
  for (Expr const* arg : e->get_arguments()) {
    Value val = evaluate_expr(eval, arg);
    args.push_back(val);
  }

  // Activate a frame for f.
  Frame* frame = eval.push_frame(fn);

  // Copy initialize each of the parameters.
  auto parms = fn->get_parameters();
  auto pi = parms.begin();
  auto ai = args.begin();
  while (pi != parms.end()) {
    Object* obj = frame->locate_local(*pi);
    obj->initialize(*ai);
    ++pi;
    ++ai;
  }

  // Recursively evaluate the function body.
  evaluate_stmt(eval, fn->get_body());

  // Get the value from the return object and return that.
  Object* ret = frame->locate_local(fn->get_return());
  assert(ret->is_initialized());
  Value rval = ret->load();

  // Re-enter the calling frame.
  eval.pop_frame();

  return rval;
}

static Value
eval_assign(Evaluation& eval, Assign_expr const* e)
{
  Value v1 = evaluate_expr(eval, e->get_child(0));
  Value v2 = evaluate_expr(eval, e->get_child(1));
  Object* obj = eval.locate_object(v1);
  obj->store(v2);
  return v1;
}

static Value
eval_value(Evaluation& eval, Value_conv const* e)
{
  Value v = evaluate_expr(eval, e->get_source());
  Object* obj = eval.locate_object(v);
  return obj->load();
}

Value
evaluate_expr(Evaluation& eval, Expr const* e)
{
  switch (e->get_kind()) {
  case Expr::bool_lit:
  case Expr::int_lit:
  case Expr::float_lit:
    return eval_literal(eval, static_cast<Literal_expr const*>(e));
  case Expr::id_expr:
    return eval_id(eval, static_cast<Id_expr const*>(e));
  case Expr::add_expr:
    return eval_binary_op(eval, e, std::plus<>{});
  case Expr::sub_expr:
    return eval_binary_op(eval, e, std::minus<>{});
  case Expr::mul_expr:
    return eval_binary_op(eval, e, std::multiplies<>{});
  case Expr::div_expr:
    return eval_binary_op(eval, e, std::divides<>{});
  case Expr::rem_expr:
    return eval_binary_op(eval, e, std::modulus<>{});
  case Expr::neg_expr:
    return eval_unary_op(eval, e, std::negate<>{});
  case Expr::rec_expr:
    return eval_unary_op(eval, e, invert<>{});
  case Expr::eq_expr:
    return eval_relation(eval, e, std::equal_to<>{});
  case Expr::ne_expr:
    return eval_relation(eval, e, std::not_equal_to<>{});
  case Expr::lt_expr:
    return eval_relation(eval, e, std::less<>{});
  case Expr::gt_expr:
    return eval_relation(eval, e, std::greater<>{});
  case Expr::le_expr:
    return eval_relation(eval, e, std::less_equal<>{});
  case Expr::ge_expr:
    return eval_relation(eval, e, std::greater_equal<>{});
  case Expr::cond_expr:
    return eval_cond(eval, static_cast<Cond_expr const*>(e));
  case Expr::and_expr:
  case Expr::or_expr:
  case Expr::not_expr:
    break;
  case Expr::assign_expr:
    return eval_assign(eval, static_cast<Assign_expr const*>(e));
  case Expr::call_expr:
    return eval_call(eval, static_cast<Call_expr const*>(e));
  case Expr::value_conv:
    return eval_value(eval, static_cast<Value_conv const*>(e));
  }
  e->debug();
  assert(false);
}

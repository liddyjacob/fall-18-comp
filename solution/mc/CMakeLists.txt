
cmake_minimum_required(VERSION 3.9)

set(CMAKE_CXX_FLAGS "-std=c++1z")

add_library(libmc
  symbol.cpp
  token.cpp
  lexer.cpp
  parser.cpp
  parser-expr.cpp
  parser-stmt.cpp
  parser-decl.cpp
  calculator.cpp
  generator.cpp
  name.cpp
  type.cpp
  type.print.cpp
  type.debug.cpp
  expr.cpp
  expr.debug.cpp
  expr.print.cpp
  stmt.cpp
  stmt.print.cpp
  stmt.debug.cpp
  decl.cpp
  decl.debug.cpp
  value.cpp
  object.cpp
  store.cpp
  frame.cpp
  printer.cpp
  evaluation.cpp
  evaluation.expr.cpp
  evaluation.stmt.cpp
  builder.cpp
  builder.expr.cpp
  builder.type.cpp
  builder.stmt.cpp
  builder.decl.cpp
  builder.conv.cpp
  builder.init.cpp
  builder.check.cpp
)

add_executable(mcc main.cpp)
target_link_libraries(mcc libmc)

add_executable(lex lex.cpp)
target_link_libraries(lex libmc)

add_executable(parse parse.cpp)
target_link_libraries(parse libmc)

add_executable(calc calc.cpp)
target_link_libraries(calc libmc)

add_executable(gen gen.cpp)
target_link_libraries(gen libmc)
